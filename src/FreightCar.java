public class FreightCar extends Car {
    public FreightCar(int unicNumber, int countOfTurnes) {
        super(unicNumber, countOfTurnes);
    }

    @Override
    public String toString() {
        return "Freight" + super.toString();
    }
}
