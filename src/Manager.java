import java.util.ArrayList;
import java.util.Scanner;
import java.util.Random;

public class Manager {
    static int unicNumber = 999;
    static Scanner sc = new Scanner(System.in);
    static Random random = new Random();

    public static void main(String[] args) {

        System.out.println("Enter count of parking places");
        Parking parking = new Parking(Integer.parseInt(sc.nextLine()), Integer.parseInt(sc.nextLine()));
        boolean isOn;
        do {
            isOn = turn(parking);
        }
        while (isOn);
    }

    private static boolean turn(Parking parking) {

        freeParking(parking);
        fillParking(parking);
        if (parking.getCountOfFreePlaces() == 0) {
            System.out.println("All places in passenger cars area are occupied. Please wait " + getMinTurns(parking.getCars()) + " turns");
        }
        if (parking.getCountOfFreightFreePlaces() == 0) {
            System.out.println("All places in freight cars area are occupied. Please wait " + getMinTurns(parking.getFreightCars()) + " turns");
        }

        System.out.println("Now you can execute command. If you want to know how to do it - type \"0\"");
        while (true) {
            int ans = Integer.parseInt(sc.nextLine());
            switch (ans) {
                case 0:
                    System.out.println("This is list of all commands:");
                    System.out.println("1.Complete turn");
                    System.out.println("2.Get count of occupied places in passenger cars area");
                    System.out.println("3.Get count of free places in passenger cars area");
                    System.out.println("4.Get count of occupied places in freight cars area");
                    System.out.println("5.Get count of free places in freight cars area");
                    System.out.println("6.Find out how much turns you have to wait for at least one free place in passenger cars area");
                    System.out.println("7.Clear parking");
                    System.out.println("8.Find out how much turns you have to wait for at least one free place in passenger cars area");
                    System.out.println("9.Get information about parking");
                    System.out.println("10.Exit");
                    break;
                case 1:
                    makeTurn(parking);
                    return true;
                case 2:
                    System.out.println(parking.getCountOfOccupiedPlaces());
                    break;
                case 3:
                    System.out.println(parking.getCountOfFreePlaces());
                    break;
                case 4:
                    System.out.println(parking.getCountOfOccupiedFreightCarPlaces());
                    break;
                case 5:
                    System.out.println(parking.getCountOfFreightFreePlaces());
                    break;
                case 6:
                    System.out.println(getMinTurns(parking.getCars()));
                    break;
                case 7:
                    clear(parking.getCars());
                    clear(parking.getFreightCars());
                    break;
                case 10:
                    return false;
                case 8:
                    System.out.println(getMinTurns(parking.getFreightCars()));
                    break;
                case 9:
                    System.out.println(parking.toString());
                    break;
                default:
                    System.out.println("Please, enter correct number of command");
                    break;
            }
        }
    }

    private static int freePlaces(ArrayList<Car> cars) {
        int countOfNewPlaces = 0;
        for (int i = 0; i < cars.size(); i++)
            if (cars.get(i) != null) {
                if (cars.get(i).getCountOfTurnes() == 0) {
                    cars.set(i, null);
                    countOfNewPlaces++;
                }
            }
        return countOfNewPlaces;
    }

    private static void freeParking(Parking parking) {
        parking.addCountOfFreePlaces(freePlaces(parking.getCars()));
        parking.addCountOfFreightFreePlaces(freePlaces(parking.getFreightCars()));
    }

    private static int getMinTurns(ArrayList<Car> cars) {
        int min = 10;
        for (Car car : cars) {
            if (car != null) {
                min = Math.min(car.getCountOfTurnes(), min);
            } else {
                min = 0;
                break;
            }
        }
        return min;
    }

    private static void makeTurn(ArrayList<Car> cars) {
        for (Car car : cars) {
            if (car != null) {
                car.setCountOfTurnes(car.getCountOfTurnes() - 1);
            }
        }
    }

    private static void makeTurn(Parking parking) {
        makeTurn(parking.getCars());
        makeTurn(parking.getFreightCars());
    }

    private static int[] createCars(ArrayList<Car> cars, int freePlaces, String typeOfCar) {

        int countOfNewCars = random.nextInt(cars.size() / 3);
        int createdCars = 0;
        for (int i = 0; i < cars.size() & freePlaces > 0 & countOfNewCars > 0; i++) {
            if (cars.get(i) == null) {
                switch (typeOfCar) {
                    case "Passenger":
                        cars.set(i, new PassengerCar(++unicNumber, random.nextInt(10) + 1));
                        break;
                    case "Freight":
                        cars.set(i, new FreightCar(++unicNumber, random.nextInt(10) + 1));
                        break;
                }
                freePlaces--;
                countOfNewCars--;
                createdCars++;
            }
            if (unicNumber == 9999) {
                unicNumber = 999;
            }
        }

        return new int[]{createdCars, countOfNewCars};
    }

    private static void fillParking(Parking parking) {
        parking.addCountOfOccupiedPlaces(createCars(parking.getCars(), parking.getCountOfFreePlaces(), "Passenger")[0]);
        int[] infOfFreightCars = createCars(parking.getFreightCars(), parking.getCountOfFreightFreePlaces(), "Freight");
        int createdFreightCars = infOfFreightCars[0];
        int waitingFreightCars = infOfFreightCars[1];
        parking.addCountOfOccupiedFreightCarPlaces(createdFreightCars);
        if (waitingFreightCars != 0) {
            freightCarToPassengerArea(parking.getCars(), waitingFreightCars);
        }
        parking.addCountOfOccupiedPlaces(infOfFreightCars[1] - waitingFreightCars);
    }

    private static void freightCarToPassengerArea(ArrayList<Car> cars, int waitingFreightCars) {
        for (int i = 0; i < cars.size() & waitingFreightCars > 0; i++) {
            if (cars.get(i) == null & cars.get(i + 1) == null) {
                cars.set(i, new FreightCar(++unicNumber, random.nextInt(10) + 1));
                cars.set(i + 1, cars.get(i));
                waitingFreightCars--;
            }
        }
    }
    private static void clear(ArrayList<Car> cars){
        for (int i = 0; i < cars.size(); i++) {
            cars.set(i, null);
        }
    }
}
