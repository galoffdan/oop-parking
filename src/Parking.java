import java.util.ArrayList;


public class Parking {
    private ArrayList<Car> cars;
    private ArrayList<Car> freightCars;
    private int countOfOccupiedPlaces;
    private int countOfOccupiedFreightCarPlaces;
    private int countOfFreePlaces;
    private int countOfFreightFreePlaces;
    private int numOfPassengerCars;
    private int numOfFreightCars;

    public int getCountOfFreePlaces() {
        return countOfFreePlaces;
    }

    public void addCountOfFreePlaces(int countOfFreePlaces) {
        this.countOfFreePlaces += countOfFreePlaces;
        this.countOfOccupiedPlaces = numOfPassengerCars - this.countOfFreePlaces;
    }

    public int getCountOfFreightFreePlaces() {
        return countOfFreightFreePlaces;
    }

    public void addCountOfFreightFreePlaces(int countOfFreightFreePlaces) {
        this.countOfFreightFreePlaces += countOfFreightFreePlaces;
        this.countOfOccupiedFreightCarPlaces = numOfFreightCars - this.countOfFreightFreePlaces;
    }

    public int getCountOfOccupiedFreightCarPlaces() {
        return countOfOccupiedFreightCarPlaces;
    }

    public void addCountOfOccupiedFreightCarPlaces(int countOfOccupiedFreightCarPlaces) {
        this.countOfOccupiedFreightCarPlaces += countOfOccupiedFreightCarPlaces;
        this.countOfFreightFreePlaces = numOfFreightCars - this.countOfOccupiedFreightCarPlaces;
    }

    public ArrayList<Car> getFreightCars() {
        return freightCars;
    }

    public void setFreightCars(ArrayList<Car> freightCars) {
        this.freightCars = freightCars;
    }

    public Parking(int numOfPassengerCars, int numOfFreightCars) {
        this.cars = new ArrayList<>(numOfPassengerCars);
        fillNull(this.cars, numOfPassengerCars);
        this.freightCars = new ArrayList<>(numOfFreightCars);
        fillNull(this.freightCars, numOfFreightCars);
        this.countOfOccupiedPlaces = 0;
        this.countOfFreePlaces = numOfPassengerCars;
        this.countOfFreightFreePlaces = numOfFreightCars;
        this.countOfOccupiedFreightCarPlaces = 0;
        this.numOfFreightCars = numOfFreightCars;
        this.numOfPassengerCars = numOfPassengerCars;
    }

    public int getCountOfOccupiedPlaces() {
        return countOfOccupiedPlaces;
    }

    public void addCountOfOccupiedPlaces(int countOfOccupiedPlaces) {
        this.countOfOccupiedPlaces += countOfOccupiedPlaces;
        this.countOfFreePlaces = numOfPassengerCars - this.countOfOccupiedPlaces;
    }

    public ArrayList<Car> getCars() {
        return cars;
    }

    public void setCars(ArrayList<Car> cars) {
        this.cars = cars;
    }

    public Parking() {
    }

    @Override
    public String toString() {
        return "Passenger cars area: \n" +
                arrToString(cars) +
                "\nFreight cars area: \n" +
                arrToString(freightCars);
    }
    private StringBuilder arrToString(ArrayList<Car> cars){
        StringBuilder str = new StringBuilder();
        int i = 1;
        for (Car car : cars) {
            str.append(i).append(". ");
            if (car != null) {
                str.append(car.toString());
            }
            str.append("\n");
            i++;
        }
        return str;
    }
    private void fillNull(ArrayList<Car> cars, int count){
        for (int i = 0; i < count; i++) {
            cars.add(null);
        }
    }
}

