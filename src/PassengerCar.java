public class PassengerCar extends Car{
    public PassengerCar(int unicNumber, int countOfTurnes) {
        super(unicNumber, countOfTurnes);
    }

    @Override
    public String toString() {
        return "Passenger" + super.toString();
    }
}
