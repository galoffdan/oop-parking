public abstract class Car {


    private int unicNumber;
    private int countOfTurnes;

    public int getUnicNumber() {
        return unicNumber;
    }

    public void setUnicNumber(int unicNumber) {
        this.unicNumber = unicNumber;
    }

    public int getCountOfTurnes() {
        return countOfTurnes;
    }

    public void setCountOfTurnes(int countOfTurnes) {
        this.countOfTurnes = countOfTurnes;
    }

    public Car(int unicNumber, int countOfTurnes) {
        this.unicNumber = unicNumber;
        this.countOfTurnes = countOfTurnes;
    }

    @Override
    public String toString() {
        return "Car{" +
                "unicNumber=" + unicNumber +
                ", countOfTurnes=" + countOfTurnes +
                '}';
    }
}
